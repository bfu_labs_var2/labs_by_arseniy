#pragma once

#define SIZE 3

#ifdef SIZE

void sortMatrix(int matrix[SIZE][SIZE]);

#endif